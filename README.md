Facilitar els càlculs per predir l'espai usat per les particions.

[[_TOC_]]

# Calcular com és fan les particions amb l'Anaconda de Red Hat
La suposició és senzilla. Només cal multiplicar tots els valors per la mateixa constant. Però hi ha un mínim i un màxim. Llavors aquests són els paràmetres a tenir en compte.

## Exemple
|nom |mínim |màxim|
|----|------|-----|
|swap| 2 GiB|4 GiB|
|/tmp| 2 GiB|2 GiB|
|/var|50 GiB|  +∞ |

Tenim els límits 1, 2 i infinit. On per 1, hi ha un total de 54 GiB, per 2 hi ha un total de 106 GiB.
